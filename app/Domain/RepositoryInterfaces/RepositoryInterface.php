<?php


namespace App\Domain\RepositoryInterfaces;


interface RepositoryInterface
{
    public function all();

    public function allActive();

    public function findById($id);

    public function update($params);

    public function delete($id);
}
