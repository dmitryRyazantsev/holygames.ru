<?php

namespace App\Domain\Models;

class Genre extends BaseDomainModel
{
    protected $table = 'genres';

    protected $fillable = [
        'name',
        'is_deleted'
    ];

    protected $guarded = [
        'id'
    ];

    protected $visible = [
        'id',
        'name',
        'is_deleted'
    ];
}
