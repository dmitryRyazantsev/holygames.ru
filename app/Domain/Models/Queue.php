<?php

namespace App\Domain\Models;

class Queue extends BaseDomainModel
{
    protected $table = 'queue';

    protected $fillable = [
        'game_id',
        'amount_users',
        'price',
        'status',
        'is_publish',
        'is_deleted'
    ];

    protected $guarded = [
        'id'
    ];

    protected $visible = [
        'id',
        'game_id',
        'amount_users',
        'price',
        'tickets',
        'game',
        'status',
        'is_publish',
        'is_deleted'
    ];

    const STATUS_IN_PROGRESS = 1,
        STATUS_CLOSED = 2;

    public function game()
    {
        return $this->belongsTo(Game::class);
    }

    public function tickets()
    {
        return $this->hasMany(Ticket::class);
    }
}
