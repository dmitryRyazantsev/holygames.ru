<?php

namespace App\Domain\Models;


use Illuminate\Database\Eloquent\Model;

class BaseDomainModel extends Model
{

    public function scopeActive($query)
    {
        return $query->where('is_deleted', '=', 0);
    }
}
