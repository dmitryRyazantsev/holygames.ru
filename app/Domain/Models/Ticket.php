<?php

namespace App\Domain\Models;

class Ticket extends BaseDomainModel
{
    protected $table = 'tickets';

    protected $fillable = [
        'queue_id',
        'status',
        'is_deleted'
    ];

    protected $guarded = [
        'id'
    ];

    protected $visible = [
        'id',
        'queue_id',
        'status',
        'is_deleted',
        'created_at'
    ];

    const STATUS_QUEUE_NOT_FILLED = 1,
        STATUS_WINNER = 2,
        STATUS_LOOSER = 3;

    public function queue()
    {
        $this->hasOne(Queue::class);
    }
}
