<?php

namespace App\Domain\Models;


class Game extends BaseDomainModel
{
    protected $table = 'games';

    protected $fillable = [
        'name',
        'price',
        'category_id',
        'genre_id',
        'description',
        'release_date',
        'vendor',
        'language',
        'shop',
        'image_preview',
        'image_view',
        'image_1',
        'image_2',
        'image_3',
        'image_4',
        'video',
        'is_deleted'
    ];

    public function genre()
    {
        return $this->belongsTo(\App\Domain\Models\Genre::class);
    }

    public function category()
    {
        return $this->belongsTo(\App\Domain\Models\Category::class);
    }

    public function allQueue()
    {
        return $this->hasMany(Queue::class)
            ->active();
    }

    public function activeQueue()
    {
        return $this->hasMany(Queue::class)
            ->active()
            ->where('is_publish', '=', 1)
            ->where('status', '=', Queue::STATUS_IN_PROGRESS);
    }
}
