<?php

namespace App\Domain\Models;


class Category extends BaseDomainModel
{
    protected $table = 'categories';

    protected $fillable = [
        'name',
        'is_deleted'
    ];

    protected $guarded = [
        'id'
    ];

    protected $visible = [
        'id',
        'name',
        'is_deleted'
    ];
}
