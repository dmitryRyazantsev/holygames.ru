<?php

namespace App\Domain\Services;


use App\Domain\Models\Queue;
use App\Domain\Models\Ticket;
use App\Domain\Repositories\QueueRepository;
use App\Domain\Repositories\TicketRepository;
use Symfony\Component\HttpKernel\Exception\HttpException;

class QueueService
{

    protected $queueRepository = null;
    protected $ticketRepository = null;

    public function __construct()
    {
        $this->queueRepository = new QueueRepository();
        $this->ticketRepository = new TicketRepository();
    }

    public function setWinner($queueId, $winnerTicketId)
    {
        $queue = $this->queueRepository->findById($queueId);

        if ($queue->amount_users != count($queue->tickets)) {
            throw new HttpException('Очередь не заполнена или не сущуствует');
        }

        foreach($queue->tickets as $ticket){
            if($ticket->id == $winnerTicketId){
                $ticket->status = Ticket::STATUS_WINNER;
                mail(
                    $ticket->email,
                    'Результаты',
                    '<p>Поздравляем! Ожидайте дальнейших инструкций.</p><hr>'
                );
            }else{
                $ticket->status = Ticket::STATUS_LOOSER;
                mail(
                    $ticket->email,
                    'Результаты',
                    '<p>Увы, вы не выйграли.</p><hr>'
                );
            }
            $this->ticketRepository->update($ticket->toArray());
        }

        $queue->is_publish = 0;
        $queue->status = Queue::STATUS_CLOSED;

        $queue = $this->queueRepository->update($queue->toArray());

        return $queue;
    }

    public function sellTicket($queueId)
    {

    }
}
