<?php

namespace App\Domain\Helpers;


use Illuminate\Http\JsonResponse;

class ApiJsonResponse extends JsonResponse{

    public function __construct($data = null, $status = 200, $headers = array())
    {
        parent::__construct($data, $status, $headers, JSON_UNESCAPED_UNICODE);
    }
}
