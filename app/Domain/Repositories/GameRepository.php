<?php

namespace App\Domain\Repositories;


use App\Domain\Models\Game;

class GameRepository extends AbstractRepository
{
    public function __construct()
    {
        $this->model = new Game();
        $this->relation = [
            'genre',
            'category',
            'allQueue.tickets'
        ];
    }

    public function getByFilters($params)
    {
        $model = $this->model->take(10);

        if(array_key_exists('page', $params)){
            $model = $model->skip(($params['page'] - 1) * 10);
        }

        if(array_key_exists('category', $params)){
            $model = $model->where('category_id', '=', $params['category']);
        }

        if(array_key_exists('genre', $params)){
            $model = $model->where('genre_id', '=', $params['genre']);
        }

        if(array_key_exists('search', $params)){
            $model = $model->where('name', 'like', '%'.$params['search'].'%');
        }

        return $model
            ->with($this->relation)
            ->get();
    }

    public function total($params = [])
    {
        $model = $this->model;

        if(array_key_exists('category', $params)){
            $model = $model->where('category_id', '=', $params['category']);
        }

        if(array_key_exists('genre', $params)){
            $model = $model->where('genre_id', '=', $params['genre']);
        }

        if(array_key_exists('search', $params)){
            $model = $model->where('name', 'like', '%'.$params['search'].'%');
        }

        return $model->count();
    }

}
