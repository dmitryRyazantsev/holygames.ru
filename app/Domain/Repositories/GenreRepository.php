<?php

namespace App\Domain\Repositories;


use App\Domain\Models\Genre;
use App\Domain\RepositoryInterfaces\GenreInterface;

class GenreRepository extends AbstractRepository implements GenreInterface
{
    public function __construct()
    {
        $this->model = new Genre();
    }
}
