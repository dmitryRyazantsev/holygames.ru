<?php

namespace App\Domain\Repositories;


use App\Domain\Models\Queue;
use App\Domain\RepositoryInterfaces\QueueInterface;

class QueueRepository extends AbstractRepository implements QueueInterface
{
    public function __construct()
    {
        $this->model = new Queue();
        $this->relation = [
            'tickets',
//            'game'
        ];
    }

}
