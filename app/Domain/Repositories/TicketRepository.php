<?php

namespace App\Domain\Repositories;


use App\Domain\Models\Ticket;
use App\Domain\RepositoryInterfaces\TicketInterface;

class TicketRepository extends AbstractRepository implements TicketInterface
{
    public function __construct()
    {
        $this->model = new Ticket();
    }

}
