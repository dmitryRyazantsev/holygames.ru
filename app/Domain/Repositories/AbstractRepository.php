<?php

namespace App\Domain\Repositories;


use App\Domain\RepositoryInterfaces\RepositoryInterface;

/**
 * Class AbstractRepository
 * @package App\Domain\Repositories
 */
class AbstractRepository implements RepositoryInterface
{
    /**
     * @var
     */
    protected $model;
    protected $relation = [];
    protected $perPage = 20;

    /**
     * @return mixed
     */
    public function all()
    {
        return $this->model
            ->active()
            ->with($this->relation)
            ->get();
    }

    /**
     * @return mixed
     */
    public function allActive()
    {
        return $this->model
            ->active()
            ->with($this->relation)
            ->get();
    }

    public function allPaginate()
    {
        return $this->model
            ->active()
            ->with($this->relation)
            ->paginate($this->perPage);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function findById($id)
    {
        return $this->model
            ->with($this->relation)
            ->find($id);
    }

    /**
     * @param $params
     * @return mixed
     */
    public function update($params)
    {
        if (array_key_exists('id', $params) && $params['id']) {
            $this->model = $this->model
                ->find($params['id'])
                ->fill($params);

            $this->model->save();

            return $this->model;
        }

        return $this->model->create($params);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        $entity = $this->model->find($id);

        if ($entity) {
            $entity->is_deleted = 1;
            $entity->save();
        }

        return $entity;
    }
}
