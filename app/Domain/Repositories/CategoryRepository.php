<?php

namespace App\Domain\Repositories;


use App\Domain\Models\Category;
use App\Domain\RepositoryInterfaces\CategoryInterface;

class CategoryRepository extends AbstractRepository implements CategoryInterface
{
    public function __construct()
    {
        $this->model = new Category();
    }
}
