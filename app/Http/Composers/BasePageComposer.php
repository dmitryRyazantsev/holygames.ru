<?php

namespace App\Http\Composers;

use App\Domain\Repositories\CategoryRepository;
use App\Domain\Repositories\GenreRepository;
use Illuminate\Contracts\View\View;

class BasePageComposer
{
    protected $categoryRepository = null;
    protected $genreRepository = null;

    public function __construct()
    {
        $this->categoryRepository = new CategoryRepository();
        $this->genreRepository = new GenreRepository();
    }

    public function compose(View $view)
    {
        $view->with([
            'categories' => $this->categoryRepository->all(),
            'genres' => $this->genreRepository->all()
        ]);
    }
}
