<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', [
    'as' => 'index',
    'uses' => 'IndexController@index'
]);


Route::get('/game/', [
    'as' => 'game.index',
    'uses' => 'GameController@index'
]);

Route::get('/game/{id}', [
    'as' => 'game.show',
    'uses' => 'GameController@show'
]);

Route::any('login', [
    'as' => 'admin.login',
    'uses' => 'Admin\AuthController@login'
]);

Route::get('payment', [
    'as' => 'payment.pay',
    'uses' => 'PaymentController@pay'
]);

Route::any('payment/fail', [
    'as' => 'payment.fail',
    'uses' => 'PaymentController@fail'
]);

Route::any('payment/success', [
    'as' => 'payment.success',
    'uses' => 'PaymentController@success'
]);

Route::get('/{page}', [
    'as' => 'page',
    'uses' => 'IndexController@page'
]);

