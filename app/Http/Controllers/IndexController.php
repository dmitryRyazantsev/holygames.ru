<?php

namespace App\Http\Controllers;


use App\Domain\Helpers\ApiJsonResponse;
use App\Domain\Models\Genre;
use App\Domain\Repositories\CategoryRepository;
use App\Domain\Repositories\GameRepository;
use App\Domain\Repositories\GenreRepository;
use Illuminate\Http\Request;
use Illuminate\Http\Response;


class IndexController extends Controller
{
    protected $gameRepository = null;

    public function __construct()
    {
        $this->gameRepository = new GameRepository();
    }

    public function index(Request $request)
    {
        $games = $this->gameRepository->getByFilters(['page' => 1]);
        $total = $this->gameRepository->total();

        return view('frontend.home')
            ->with([
                'games' => $games,
                'page' => 1,
                'total' => $total
            ]);
    }

    public function page($page)
    {
        $path = base_path('resources/views/frontend/pages/'.$page.'.blade.php');

        if(!file_exists($path)){
            return \Response::make('', 404);
        }
        $view = 'frontend.pages.'.$page;

        return view($view);
    }
}
