<?php

namespace App\Http\Controllers\Admin;

use App\Domain\Helpers\ApiJsonResponse;
use App\Domain\Repositories\GameRepository;
use App\Domain\Repositories\QueueRepository;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class GameQueueController extends Controller
{

    protected $gameRepository = null;
    protected $queueRepository = null;

    public function __construct()
    {
        $this->gameRepository = new GameRepository();
        $this->queueRepository = new QueueRepository();
    }

    public function index($gameId)
    {
        return view('admin.game.queue.list')
            ->with([
                'model' => $this->gameRepository->findById($gameId)
            ]);
    }

    public function create($gameId)
    {
        return view('admin.game.queue.create')
            ->with([
                'game' => $this->gameRepository->findById($gameId)
            ]);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $entity = $this->queueRepository->update($data);

        return new ApiJsonResponse($entity);
    }

    public function edit($gameId, $queueId)
    {
        return view('admin.game.queue.edit')
            ->with([
                'model' => $this->queueRepository->findById($queueId),
                'game' => $this->gameRepository->findById($gameId)
            ]);
    }

    public function update(Request $request, $gameId, $queueId)
    {
        $data = $request->all();
        $entity = $this->queueRepository->update($data);

        return new ApiJsonResponse($entity);
    }

    public function destroy($gameId, $queueId)
    {
        $entity = $this->queueRepository->delete($queueId);

        return new ApiJsonResponse($entity);
    }

    public function tickets($queueId)
    {
        return view('admin.game.queue.tickets')
            ->with([
                'model' => $this->queueRepository->findById($queueId)
            ]);
    }
}
