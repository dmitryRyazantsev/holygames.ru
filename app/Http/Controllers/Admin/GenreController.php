<?php

namespace App\Http\Controllers\Admin;

use App\Domain\Helpers\ApiJsonResponse;
use App\Domain\Repositories\GenreRepository;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class GenreController extends Controller
{

    protected $genreRepository = null;

    public function __construct()
    {
        $this->genreRepository = new GenreRepository();
    }

    public function index()
    {
        return view('admin.genre.list')
            ->with([
                'genres' => $this->genreRepository->all()
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.genre.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $entity = $this->genreRepository->update($data);

        return new ApiJsonResponse($entity);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        return view('admin.genre.edit')
            ->with([
                'model' => $this->genreRepository->findById($id)
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $entity = $this->genreRepository->update($data);

        return new ApiJsonResponse($entity);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $entity = $this->genreRepository->delete($id);

        return new ApiJsonResponse($entity);
    }
}
