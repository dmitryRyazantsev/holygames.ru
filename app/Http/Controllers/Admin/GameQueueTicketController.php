<?php

namespace App\Http\Controllers\Admin;

use App\Domain\Helpers\ApiJsonResponse;
use App\Domain\Repositories\QueueRepository;
use App\Domain\Repositories\TicketRepository;
use App\Domain\Services\QueueService;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class GameQueueTicketController extends Controller
{

    protected $ticketRepository = null;
    protected $queueRepository = null;
    protected $queueService = null;

    public function __construct()
    {
        $this->ticketRepository = new TicketRepository();
        $this->queueRepository = new QueueRepository();
        $this->queueService = new QueueService();
    }

    public function index($gameId, $queueId)
    {
        return view('admin.game.queue.tickets')
            ->with([
                'model' => $this->queueRepository->findById($queueId)
            ]);
    }

    public function winner(Request $request, $gameId, $queueId, $ticketId)
    {
        $this->queueService->setWinner($queueId, $ticketId);

        return \Redirect::route('admin.game.queue.index', [$gameId, $queueId]);
    }

}
