<?php

namespace App\Http\Controllers\Admin;

use App\Domain\Helpers\ApiJsonResponse;
use App\Domain\Repositories\QueueRepository;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class QueueController extends Controller
{

    protected $queueRepository = null;

    public function __construct()
    {
        $this->queueRepository = new QueueRepository();
    }

    public function index()
    {
        return view('admin.queue.list')
            ->with([
                'queues' => $this->queueRepository->all()
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.queue.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $entity = $this->queueRepository->update($data);

        return new ApiJsonResponse($entity);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        return view('admin.queue.edit')
            ->with([
                'model' => $this->queueRepository->findById($id)
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $entity = $this->queueRepository->update($data);

        return new ApiJsonResponse($entity);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $entity = $this->queueRepository->delete($id);

        return new ApiJsonResponse($entity);
    }
}
