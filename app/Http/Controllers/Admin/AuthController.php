<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function login()
    {
        if (\Request::method() == 'POST') {
            $email = \Input::get('email');
            $password = \Input::get('password');

            if (
                Auth::attempt(
                    [
                        'email' => $email,
                        'password' => $password,
                    ],
                    true
                )
            ) {
                return \Redirect::route('admin.index');
            }
        }

        return view('admin.login');
    }

    public function logout()
    {
        Auth::logout();

        return \Redirect::route('index');
    }
}
