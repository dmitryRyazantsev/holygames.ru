<?php

namespace App\Http\Controllers\Admin;

use App\Domain\Helpers\ApiJsonResponse;
use App\Domain\Repositories\CategoryRepository;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{

    protected $categoryRepository = null;

    public function __construct()
    {
        $this->categoryRepository = new CategoryRepository();
    }

    public function index()
    {
        return view('admin.category.list')
            ->with([
                'categories' => $this->categoryRepository->all()
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $entity = $this->categoryRepository->update($data);

        return new ApiJsonResponse($entity);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        return view('admin.category.edit')
            ->with([
                'model' => $this->categoryRepository->findById($id)
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $entity = $this->categoryRepository->update($data);

        return new ApiJsonResponse($entity);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $entity = $this->categoryRepository->delete($id);

        return new ApiJsonResponse($entity);
    }
}
