<?php

namespace App\Http\Controllers\Admin;

use App\Domain\Helpers\ApiJsonResponse;
use App\Domain\Repositories\CategoryRepository;
use App\Domain\Repositories\GameRepository;
use App\Domain\Repositories\GenreRepository;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class GameController extends Controller
{

    protected $gameRepository = null;

    public function __construct()
    {
        $this->gameRepository = new GameRepository();
    }

    public function index()
    {
        return view('admin.game.list')
            ->with([
                'games' => $this->gameRepository->allPaginate(),
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.game.create')
            ->with([
                'categories' => (new CategoryRepository())->all(),
                'genres' => (new GenreRepository())->all(),
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $entity = $this->gameRepository->update($data);

        return new ApiJsonResponse($entity);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        return view('admin.game.edit')
            ->with([
                'model' => $this->gameRepository->findById($id),
                'categories' => (new CategoryRepository())->all(),
                'genres' => (new GenreRepository())->all(),
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $entity = $this->gameRepository->update($data);

        return new ApiJsonResponse($entity);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $entity = $this->gameRepository->delete($id);

        return new ApiJsonResponse($entity);
    }
}
