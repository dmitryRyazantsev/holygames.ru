<?php

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class IndexController extends Controller
{
    public function __construct()
    {

    }

    public function index()
    {
        return Redirect::route('admin.game.index');
    }
}
