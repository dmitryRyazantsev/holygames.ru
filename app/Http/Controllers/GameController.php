<?php

namespace App\Http\Controllers;


use App\Domain\Models\Genre;
use App\Domain\Repositories\CategoryRepository;
use App\Domain\Repositories\GameRepository;
use App\Domain\Repositories\GenreRepository;
use Illuminate\Http\Request;

class GameController extends Controller
{
    protected $gameRepository = null;

    public function __construct()
    {
        $this->gameRepository = new GameRepository();
    }

    public function index(Request $request)
    {
        $params = $request->all() ? $request->all() : [];
        $page = array_key_exists('page', $params) ? $params['page'] : 1;

        $games = $this->gameRepository->getByFilters($params);

        $total = $this->gameRepository->total($params);

        return view('frontend.games')
            ->with([
                'games' => $games,
                'params' => $params,
                'total' => $total,
                'page' => $page
            ]);
    }

    public function show($id)
    {
        return view('frontend.show')
            ->with([
                'game' => $this->gameRepository->findById($id)
            ]);
    }
}
