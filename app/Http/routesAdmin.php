<?php

Route::group(
    [
        'prefix' => 'admin',
        'middleware' => ['auth']
    ],
    function () {

        Route::get('/', [
            'as' => 'admin.index',
            'uses' => 'Admin\IndexController@index'
        ]);

        Route::resource('category', 'Admin\CategoryController',[ 'except' => [ 'show' ]]);
        Route::resource('genre', 'Admin\GenreController',[ 'except' => [ 'show' ]]);

        Route::get('game/{gameId}/queue/{queueId}/ticket/{ticketId}', [
            'as' => 'admin.game.queue.ticket',
            'uses' => 'Admin\GameQueueTicketController@winner'
        ]);

        Route::resource('game.queue.ticket', 'Admin\GameQueueTicketController',[ 'except' => [ 'show', 'create', 'store']]);
        Route::resource('game.queue', 'Admin\GameQueueController',[ 'except' => [ 'show' ]]);
        Route::resource('game', 'Admin\GameController',[ 'except' => [ 'show' ]]);

        Route::get('logout', [
            'as' => 'admin.logout',
            'uses' => 'Admin\AuthController@logout'
        ]);
    });
