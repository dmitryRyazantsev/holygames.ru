'use strict'

var app = angular.module('AdminModule',[
    'GenreModule',
    'CategoryModule',
    'GameModule',
    'QueueModule',
]);

app.config(function config(
    $interpolateProvider
){
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
});
