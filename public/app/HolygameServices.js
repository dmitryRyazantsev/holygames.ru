'use strict';

var HolygameServices = angular.module('HolygameServices', []);

HolygameServices.config(function config(
    RestangularProvider
){
    var url = location.origin + '/admin/'
    RestangularProvider.setBaseUrl(url);
});
