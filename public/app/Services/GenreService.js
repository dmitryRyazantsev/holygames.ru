'use strict'

HolygameServices.service('GenreService', function(
    $rootScope,
    Restangular
){
    this.delete = function(genreId) {
        return Restangular
            .one('genre', genreId)
            .remove()
            .then(function(){
                $rootScope.$broadcast('genre.deleted');
            });
    }

    this.update = function(genreId, params){
        var genre = Restangular.copy(params);

        genre.route = 'genre/';

        return genre
            .put()
            .then(function(){
                $rootScope.$broadcast('genre.updated');
        });
    }

    this.create = function(params){
        var genre = Restangular.copy({});

        genre.route = '';

        return genre
            .post('genre', params)
            .then(function(){
                $rootScope.$broadcast('genre.created');
        });
    }
});
