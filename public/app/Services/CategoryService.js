'use strict'

HolygameServices.service('CategoryService', function(
    $rootScope,
    Restangular
){
    this.delete = function(categoryId) {
        return Restangular
            .one('category', categoryId)
            .remove()
            .then(function(){
                $rootScope.$broadcast('category.deleted');
            });
    }

    this.update = function(categoryId, params){
        var category = Restangular.copy(params);

        category.route = 'category/';

        return category
            .put()
            .then(function(){
                $rootScope.$broadcast('category.updated');
        });
    }

    this.create = function(params){
        var category = Restangular.copy({});

        category.route = '';

        return category
            .post('category', params)
            .then(function(){
                $rootScope.$broadcast('category.created');
        });
    }
});
