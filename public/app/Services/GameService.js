
'use strict'

HolygameServices.service('GameService', function(
    $rootScope,
    Restangular
){
    this.delete = function(gameId) {
        return Restangular
            .one('game', gameId)
            .remove()
            .then(function(){
                $rootScope.$broadcast('game.deleted');
            });
    }

    this.update = function(gameId, params){
        var game = Restangular.copy(params);

        game.route = 'game/';

        return game
            .put()
            .then(function(){
                $rootScope.$broadcast('game.updated');
        });
    }

    this.create = function(params){
        var game = Restangular.copy({});

        game.route = '';

        return game
            .post('game', params)
            .then(function(){
                $rootScope.$broadcast('game.created');
        });
    }
});
