'use strict'

HolygameServices.service('QueueService', function(
    $rootScope,
    Restangular
){
    this.delete = function(gameId, queueId) {
        return Restangular
            .one('game', gameId)
            .one('queue', queueId)
            .remove()
            .then(function(){
                $rootScope.$broadcast('queue.deleted');
            });
    }

    this.update = function(gameId, queueId, params){
        var queue = Restangular.copy(params);

        queue.route = 'game/'+gameId+'/queue/';

        return queue
            .put()
            .then(function(){
                $rootScope.$broadcast('queue.updated');
        });
    }

    this.create = function(params){
        var queue = Restangular.copy({});

        queue.route = 'game/' + params['game_id'];

        return queue
            .post('queue', params)
            .then(function(){
                $rootScope.$broadcast('queue.created');
        });
    }
});
