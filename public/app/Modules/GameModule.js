'use strict';

var GameModule = angular.module("GameModule", [
    'restangular',
    'HolygameServices',
]);

GameModule.config(function(
    RestangularProvider
){});

GameModule.controller("GameListCtrl", function(
    $rootScope,
    $scope,
    $window,
    GameService
){
    $scope.games = games;

    $rootScope.$on('game.deleted', function(){
        $window.location.reload();
    });

    $scope.confirm = function($index){
        $scope.games[$index].confirmed = true;
    }

    $scope.delete = function(gameId){
        GameService.delete(gameId);
    }
});

GameModule.controller('GameEditCtrl', function(
    $scope,
    $rootScope,
    $window,
    GameService
){
    $scope.model = model;
    $scope.genres = genres;
    $scope.categories = categories;

    $rootScope.$on('game.updated', function(){
        $window.location.href = '/admin/game'
    });

    $scope.update = function(){

        if($scope.model.id &&
            $scope.model.name
        ){
            GameService.update($scope.model.id, $scope.model);
        }else{
            alert('Заполните все поля пожалуйста');
        }
    }
});

GameModule.controller('GameCreateCtrl', function(
    $scope,
    $rootScope,
    $window,
    GameService
){
    $scope.genres = genres;
    $scope.categories = categories;

    $rootScope.$on('game.created', function(){
        $window.location.href = '/admin/game'
    });

    $scope.update = function(){

        if($scope.model.name){
            GameService.create($scope.model);
        }else{
            alert('Заполните все поля пожалуйста');
        }
    }
});
