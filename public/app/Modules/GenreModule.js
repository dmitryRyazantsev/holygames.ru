'use strict';

var GenreModule = angular.module("GenreModule", [
    'restangular',
    'HolygameServices',
]);

GenreModule.config(function(
    RestangularProvider
){});

GenreModule.controller("GenreListCtrl", function(
    $rootScope,
    $scope,
    $window,
    GenreService
){
    $scope.genres = genres;

    $rootScope.$on('genre.deleted', function(){
        $window.location.reload();
    });

    $scope.confirm = function($index){
        $scope.genres[$index].confirmed = true;
    }

    $scope.delete = function(genreId){
        GenreService.delete(genreId);
    }
});

GenreModule.controller('GenreEditCtrl', function(
    $scope,
    $rootScope,
    $window,
    GenreService
){
    $scope.model = model;

    $rootScope.$on('genre.updated', function(){
        $window.location.href = '/admin/genre'
    });

    $scope.update = function(){

        if($scope.model.id &&
            $scope.model.name
        ){
            GenreService.update($scope.model.id, $scope.model);
        }else{
            alert('Заполните все поля пожалуйста');
        }
    }
});

GenreModule.controller('GenreCreateCtrl', function(
    $scope,
    $rootScope,
    $window,
    GenreService
){
    $rootScope.$on('genre.created', function(){
        $window.location.href = '/admin/genre'
    });

    $scope.update = function(){

        if($scope.model.name){
            GenreService.create($scope.model);
        }else{
            alert('Заполните все поля пожалуйста');
        }
    }
});
