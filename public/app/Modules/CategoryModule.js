'use strict';

var CategoryModule = angular.module("CategoryModule", [
    'restangular',
    'HolygameServices',
]);

CategoryModule.config(function(
    RestangularProvider
){});

CategoryModule.controller("CategoryListCtrl", function(
    $rootScope,
    $scope,
    $window,
    CategoryService
){
    $scope.categories = categories;

    $rootScope.$on('category.deleted', function(){
        $window.location.reload();
    });

    $scope.confirm = function($index){
        $scope.categories[$index].confirmed = true;
    }

    $scope.delete = function(categoryId){
        CategoryService.delete(categoryId);
    }
});

CategoryModule.controller('CategoryEditCtrl', function(
    $scope,
    $rootScope,
    $window,
    CategoryService
){
    $scope.model = model;

    $rootScope.$on('category.updated', function(){
        $window.location.href = '/admin/category'
    });

    $scope.update = function(){

        if($scope.model.id &&
            $scope.model.name
        ){
            CategoryService.update($scope.model.id, $scope.model);
        }else{
            alert('Заполните все поля пожалуйста');
        }
    }
});

CategoryModule.controller('CategoryCreateCtrl', function(
    $scope,
    $rootScope,
    $window,
    CategoryService
){
    $rootScope.$on('category.created', function(){
        $window.location.href = '/admin/category'
    });

    $scope.update = function(){

        if($scope.model.name){
            CategoryService.create($scope.model);
        }else{
            alert('Заполните все поля пожалуйста');
        }
    }
});
