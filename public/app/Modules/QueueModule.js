'use strict';

var QueueModule = angular.module("QueueModule", [
    'restangular',
    'HolygameServices',
]);

QueueModule.config(function(
    RestangularProvider
){});

QueueModule.controller("QueueListCtrl", function(
    $rootScope,
    $scope,
    $window,
    QueueService
){
    $scope.queues = queues;

    $rootScope.$on('queue.deleted', function(){
        $window.location.reload();
    });

    $scope.confirm = function($index){
        $scope.queues[$index].confirmed = true;
    }

    $scope.delete = function(gameId, queueId){
        QueueService.delete(gameId, queueId);
    }
});

QueueModule.controller('QueueEditCtrl', function(
    $scope,
    $rootScope,
    $window,
    QueueService
){
    $scope.model = model;
    $scope.game = game;

    $rootScope.$on('queue.updated', function(){
        $window.location.href = '/admin/game/'+$scope.model.game_id+'/queue'
    });

    $scope.update = function(){

        if($scope.model.id){
            QueueService.update($scope.model.game_id, $scope.model.id, $scope.model);
        }else{
            alert('Заполните все поля пожалуйста');
        }
    }
});

QueueModule.controller('QueueCreateCtrl', function(
    $scope,
    $rootScope,
    $window,
    QueueService
){
    $scope.game = game;
    $scope.model = {
        game_id: game.id
    };

    $rootScope.$on('queue.created', function(){
        $window.location.href = '/admin/game/'+$scope.model.game_id+'/queue'
    });

    $scope.update = function(){
        QueueService.create($scope.model);
    }
});
