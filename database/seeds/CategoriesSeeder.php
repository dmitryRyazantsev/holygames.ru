<?php

use Illuminate\Database\Seeder;

class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('categories')->delete();

        \App\Domain\Models\Category::create([
            'name' => 'Новинки'
        ]);

        \App\Domain\Models\Category::create([
            'name' => 'Хиты'
        ]);

        \App\Domain\Models\Category::create([
            'name' => 'Акции и Скидки'
        ]);

        \App\Domain\Models\Category::create([
            'name' => 'Мультиплеер'
        ]);

        \App\Domain\Models\Category::create([
            'name' => 'Кооператив'
        ]);

        \App\Domain\Models\Category::create([
            'name' => 'Одиночные'
        ]);

        \App\Domain\Models\Category::create([
            'name' => 'Предзаказ'
        ]);
    }
}
