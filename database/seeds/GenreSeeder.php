<?php

use Illuminate\Database\Seeder;

class GenreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('genres')->delete();

        \App\Domain\Models\Genre::create([
            'name' => 'Экшены'
        ]);

        \App\Domain\Models\Genre::create([
            'name' => 'Шутеры'
        ]);

        \App\Domain\Models\Genre::create([
            'name' => 'Приключения'
        ]);

        \App\Domain\Models\Genre::create([
            'name' => 'RPG'
        ]);

        \App\Domain\Models\Genre::create([
            'name' => 'Стратегии'
        ]);

        \App\Domain\Models\Genre::create([
            'name' => 'Симуляторы'
        ]);

        \App\Domain\Models\Genre::create([
            'name' => 'Инди игры'
        ]);

        \App\Domain\Models\Genre::create([
            'name' => 'Гонки'
        ]);

        \App\Domain\Models\Genre::create([
            'name' => 'MMORPG'
        ]);
    }
}
