<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateGamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('games', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('genre_id')->nullable()->index();
            $table->integer('category_id')->nullable()->index();
            $table->string('name');
//            $table->string('uri');
            $table->integer('price')->default(0);
            $table->text('description')->nullable();
            $table->string('release_date')->nullable();
            $table->string('vendor')->nullable();
            $table->string('language')->nullable();
            $table->string('shop')->nullable();
            $table->string('image_preview')->nullable();
            $table->string('image_view')->nullable();
            $table->string('image_1')->nullable();
            $table->string('image_2')->nullable();
            $table->string('image_3')->nullable();
            $table->string('image_4')->nullable();
            $table->string('video')->nullable();
            $table->tinyInteger('is_deleted')->default(0);
            $table->timestamps();
        });

        Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->tinyInteger('is_deleted')->default(0);
            $table->timestamps();
        });

        Schema::create('genres', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->tinyInteger('is_deleted')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('games');
        Schema::drop('categories');
        Schema::drop('genres');
    }
}
