<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddQueueAndTickets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('queue', function(Blueprint $table){
            $table->increments('id');
            $table->integer('game_id', false, true)->index();
            $table->foreign('game_id')->references('id')->on('games');
            $table->integer('amount_users');
            $table->decimal('price')->default('0.00');
            $table->integer('status')->default(1);
            $table->tinyInteger('is_publish')->default(0);
            $table->tinyInteger('is_deleted')->default(0);
            $table->timestamps();
        });

        Schema::create('tickets', function(Blueprint $table){
            $table->increments('id');
            $table->integer('queue_id', false, true)->index();
            $table->foreign('queue_id')->references('id')->on('queue');
            $table->string('email')->index();
            $table->integer('status')->default(1);
            $table->tinyInteger('is_deleted')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tickets');
        Schema::drop('queue');
    }
}
