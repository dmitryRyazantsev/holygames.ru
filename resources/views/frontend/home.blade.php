@extends('layout.main')
@section('content')
<center><h1>Покупай игры вскладчину<br>по невероятно низким ценам</h1></center>
    <p style="color:#585858;font-size:.8em;">Найдено игр: {{ $total }}</p>

    @foreach($games as $game)
    <div class="game">
        <a href="{{ route('game.show', [$game->id]) }}">
            <div style="width:100%;height:200px;overflow:hidden;">
                <img src="{{ $game->image_preview }}" style="min-height:200px;">
            </div>
            <br>
            <p><span>{{ $game->name }}</span><span>от {{ $game->price }} Р</span></p>
        </a>
    </div>
    @endforeach

<div style="width:100%;float:left;">
    <center>
        @include('layout.pagination', [
           'route' => 'game',
           'params' => [],
           'currentPage' => 1
           ])
    </center>
</div>

@overwrite
