@extends('layout.main')
@section('page-title')
    {{ $game->name }} - Holygames.ru
@overwrite
@section('content')
<div style="width:100%;overflow:hidden;height:320px;" id="img">
    <img src="{{ $game->image_view }}" style="width:100%;">
</div>

<h1 class="forgame"
    style="font-size:56px;line-height:60px;width:729px;">{{ $game->name }}</h1>

<div class="tabs">
    <input id="tab1" type="radio" name="tabs" checked>
    <label for="tab1" title="Купить">Купить</label>

    <input id="tab2" type="radio" name="tabs">
    <label for="tab2" title="Информация об игре">Информация об игре</label>

    <section id="content1">
        <p style="font-size:12px;margin-top:-10px;margin-left:40px;margin-right:130px;text-align:right;">Заполненность / Размер группы, чел.</p>
        <table>
            <tr>
                <td valign="top" style="width:100%;font-size:14px;">
                    <p>После заполнения групп в течении 1 часа случайным образом определяется побелитель, которому на указанную при оплате почту высылаются все данные для активации игры (ключи, коды и тд.)</p>
                    <p>При оплате указывайте ваш настоящий E-mail.<br>При успешной оплате он автоматически попадет в группу, уведомление об этом придет вам на указанную почту. <br>В одной группе можно выкупать несколько слотов (вплоть до всех).</p>
                    <p>Мы зарабатываем исключительно на небольшой комиссии (если выкупить все слоты в группе, то вы точно получите игру, просто она выйдет дороже на 10% чем в магазине).</p>
                </td>
                <td valign="top" style="width:260px;">
                    <table style="width:260px;float:right;" cellspacing="1">
                        @foreach($game->activeQueue as $queue)
                        <tr class="list">
                            <td>
                                <a href="/game/reg?id={{ $queue->id }}">
                                    {{ count($queue->tickets) }}/{{ $queue->amount_users }}
                                </a>
                            </td>
                            <td style="width:100%;padding:0;" >
                                <a class="b" style="float:right;" href="/game/reg?id={{ $queue->id }}">
                                    Купить за {{ $queue->price }} Р
                                </a>
                            </td>
                        </tr>
                        @endforeach
                    </table>
                </td>
            </tr>
        </table>

    </section>
    <section id="content2">

        <table>
            <tr>
                <td valign="top" style="width:100%;font-size:14px;">
                    <p>{{ $game->description }}</p>
                </td>
                <td valign="top" style="width:260px;">
                    <table style="width:260px;float:right;font-size:14px !important;">
                        <tr class="list" style="height:30px !important;">
                            <td style="width:100%;">Дата выхода:</td>
                            <td style="text-align:right;font-size:14px;">
                                <b>{{ $game->release_date }}</b>
                            </td>
                        </tr>
                        <tr class="list" style="height:30px; !important">
                            <td>Издатель:</td>
                            <td style="text-align:right;font-size:14px;">
                                <b>{{ $game->vendor }}</b>
                            </td>
                        </tr>
                        <tr class="list" style="height:30px; !important">
                            <td>Жанр:</td>
                            <td style="text-align:right;font-size:14px;">
                                <b>{{ $game->genre->name }}</b>
                            </td>
                        </tr>
                        <tr class="list" style="height:30px; !important">
                            <td>Язык:</td>
                            <td style="text-align:right;font-size:14px;">
                                <b>{{ $game->language }}</b>
                            </td>
                        </tr>
                        <tr class="list" style="height:30px; !important">
                            <td>Магазин:</td>
                            <td style="text-align:right;font-size:14px;">
                                <b>{{ $game->shop }}</b>
                            </td>
                        </tr>
                    </table>
            </tr>
        </table>
        <br><br>
        <table border="0" class="mediadesc">
            <tr>
                <td colspan="2" rowspan="2" class="td" style="width:364px !important;">
                    <iframe
                            width="364"
                            height="206"
                            src="https://www.youtube.com/embed/{{ $game->video }}>"
                            frameborder="0"
                            allowfullscreen>
                    </iframe>
                </td>
                <td style="padding-bottom:4px;">
                    <img src="{{ $game->image_1 }}">
                </td>
                <td style="padding-bottom:4px;">
                    <img src="{{ $game->image_2 }}">
                </td>
            </tr>
            <tr>
                <td>
                    <img src="{{ $game->image_3 }}">
                </td>
                <td>
                    <img src="{{ $game->image_4 }}">
                </td>
            </tr>
        </table>
    </section>
</div>
<!-- Put this script tag to the <head> of your page --><br><br>
<script type="text/javascript" src="//vk.com/js/api/openapi.js?117"></script>

<script type="text/javascript">
    VK.init({apiId: 5052826, onlyWidgets: true});
</script>

<!-- Put this div tag to the place, where the Comments block will be -->
<div id="vk_comments"></div>
<script type="text/javascript">
    VK.Widgets.Comments("vk_comments", {limit: 10, width: document.getElementById("img").offsetWidth, attach: "*"});
</script>
</div>
@overwrite
