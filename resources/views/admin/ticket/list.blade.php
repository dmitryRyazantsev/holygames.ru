@extends('AdminLTE.layout')
@section('page-header')
    Очередь {{ $model->game->name }} ({{ $model->amount_users }}) - Билеты
@overwrite
@section('page-description')
    Все
@overwrite
@section('content')
    <div class='row'>
        <div class='col-md-8'>
            <!-- Box -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Билеты</h3>

                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                        <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="box-body"
                        ng-controller="QueueTicketListCtrl">
                    <table id="genreTable"
                           class="table table-striped table-bordered"
                           cellspacing="0"
                           width="100%">
                        <thead>
                            <tr>
                                <th>id</th>
                                <th>Email</th>
                                <th>Дата</th>
                                <th>Победитель</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($model->tickets as $key => $ticket)
                                <tr>
                                    <td>{{ $ticket->id }}</td>
                                    <td>{{ $ticket->email }}</td>
                                    <td>
                                        <a href="{{ route('admin.ticket.edit', [ $ticket->id ]) }}">
                                            <button class="btn btn-primary btn-flat">Изменить</button>
                                        </a>
                                    </td>
                                    <td>
                                        <a class="btn btn-danger"
                                           ng-click="confirm({{ $key }})">Удалить
                                        </a>
                                        <a class="btn btn-danger"
                                           ng-show="tickets[{{ $key }}].confirmed"
                                           ng-click="delete({{ $ticket->id }})">
                                            Ок
                                        </a>
                                    </td>

                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
@overwrite
@section('scripts')
<script>
    var genres = {!! json_encode($genres) !!}
</script>
@overwrite
