@extends('AdminLTE.layout')
@section('page-header')
    Жанры
@overwrite
@section('page-description')
    Все Жанры
@overwrite
@section('content')
    <div class='row'>
        <div class='col-md-8'>
            <!-- Box -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Жанры</h3>
                    <a href="{{ route('admin.genre.create') }}"
                       class="btn btn-info btn-sm"
                       style="margin-left: 1em;">
                        Создать
                    </a>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                        <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="box-body"
                        ng-controller="GenreListCtrl">
                    <table id="genreTable"
                           class="table table-striped table-bordered"
                           cellspacing="0"
                           width="100%">
                        <thead>
                            <tr>
                                <th>id</th>
                                <th>Название</th>
                                <th>Изменить</th>
                                <th>Удалить</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($genres as $key => $genre)
                                <tr>
                                    <td>{{ $genre->id }}</td>
                                    <td>{{ $genre->name }}</td>
                                    <td>
                                        <a href="{{ route('admin.genre.edit', [ $genre->id ]) }}">
                                            <button class="btn btn-primary btn-flat">Изменить</button>
                                        </a>
                                    </td>
                                    <td>
                                        <a class="btn btn-danger"
                                           ng-click="confirm({{ $key }})">Удалить
                                        </a>
                                        <a class="btn btn-danger"
                                           ng-show="genres[{{ $key }}].confirmed"
                                           ng-click="delete({{ $genre->id }})">
                                            Ок
                                        </a>
                                    </td>

                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
@overwrite
@section('scripts')
<script>
    var genres = {!! json_encode($genres) !!}
</script>
@overwrite
