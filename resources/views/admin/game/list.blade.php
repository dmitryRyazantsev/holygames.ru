@extends('AdminLTE.layout')
@section('page-header')
    Игры
@overwrite
@section('page-description')
    Все Игры
@overwrite
@section('content')
    <div class='row'>
        <div class='col-md-8'>
            <!-- Box -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Игры</h3>
                    <a href="{{ route('admin.game.create') }}"
                       class="btn btn-info btn-sm"
                       style="margin-left: 1em;">
                        Создать
                    </a>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                        <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="box-body"
                        ng-controller="GameListCtrl">
                    <table id="gameTable"
                           class="table table-striped table-bordered"
                           cellspacing="0"
                           width="100%">
                        <thead>
                            <tr>
                                <th>id</th>
                                <th>Название</th>
                                <th>Цена</th>
                                <th>Очереди</th>
                                <th>Изменить</th>
                                <th>Удалить</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($games as $key => $game)
                                <tr>
                                    <td>{{ $game->id }}</td>
                                    <td>{{ $game->name }}</td>
                                    <td>{{ $game->price }} руб</td>
                                    <td>
                                        <a href="{{ route('admin.game.queue.index', [ $game->id ]) }}">
                                            <button class="btn btn-primary btn-flat">Очереди</button>
                                        </a>
                                    </td>
                                    <td>
                                        <a href="{{ route('admin.game.edit', [ $game->id ]) }}">
                                            <button class="btn btn-primary btn-flat">Изменить</button>
                                        </a>
                                    </td>
                                    <td>
                                        <a class="btn btn-danger"
                                           ng-click="confirm({{ $key }})">Удалить
                                        </a>
                                        <a class="btn btn-danger"
                                           ng-show="games[{{ $key }}].confirmed"
                                           ng-click="delete({{ $game->id }})">
                                            Ок
                                        </a>
                                    </td>

                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {!! $games->render() !!}
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
@overwrite
@section('scripts')
<script>
    var games = {!! json_encode($games) !!}
</script>
@overwrite
