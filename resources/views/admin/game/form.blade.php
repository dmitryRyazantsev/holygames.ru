<form class="form-horizontal" role="form" ng-submit="update()">
    <div class="form-group">
        <label class="col-sm-3 control-label">Название</label>
        <div class="col-sm-9">
            <input
                    type="text"
                    class="form-control"
                    {{--placeholder="Название"--}}
                    ng-model="model.name">
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-3 control-label">Цена</label>
        <div class="col-sm-9">
            <input
                    type="text"
                    class="form-control"
                    {{--placeholder="Цена"--}}
                    ng-model="model.price">
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-3 control-label">Описание</label>
        <div class="col-sm-9">
            <textarea
                    type="text"
                    class="form-control"
                    {{--placeholder="Описание"--}}
                    ng-model="model.description">
            </textarea>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-3 control-label">Дата выхода</label>
        <div class="col-sm-9">
            <input
                    type="text"
                    class="form-control"
                    ng-model="model.release_date">
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-3 control-label">Издататель</label>
        <div class="col-sm-9">
            <input
                    type="text"
                    class="form-control"
                    ng-model="model.vendor">
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-3 control-label">Жанр</label>
        <div class="col-sm-9">
            <select
                    type="text"
                    class="form-control"
                    ng-model="model.genre_id">
                <option
                        value="[[ genre.id  ]]"
                        ng-repeat="genre in genres"
                        ng-selected="genre.id == model.genre_id">
                    [[ genre.name ]]
                </option>
            </select>
        </div>
    </div>
    
    <div class="form-group">
        <label class="col-sm-3 control-label">Категория</label>
        <div class="col-sm-9">
            <select
                    type="text"
                    class="form-control"
                    ng-model="model.category_id">
                <option
                        value="[[ category.id ]]"
                        ng-repeat="category in categories"
                        ng-selected="category.id == model.category_id">
                    [[ category.name ]]
                </option>
            </select>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-3 control-label">Язык</label>
        <div class="col-sm-9">
            <input
                    type="text"
                    class="form-control"
                    ng-model="model.language">
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-3 control-label">Магазин</label>
        <div class="col-sm-9">
            <input
                    type="text"
                    class="form-control"
                    ng-model="model.shop">
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-3 control-label">Обложка</label>
        <div class="col-sm-9">
            <input
                    type="text"
                    class="form-control"
                    ng-model="model.image_preview">
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-3 control-label">Изображение шапки</label>
        <div class="col-sm-9">
            <input
                    type="text"
                    class="form-control"
                    ng-model="model.image_view">
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-3 control-label">URL картинка 1</label>
        <div class="col-sm-9">
            <input
                    type="text"
                    class="form-control"
                    ng-model="model.image_1">
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-3 control-label">URL картинка 2</label>
        <div class="col-sm-9">
            <input
                    type="text"
                    class="form-control"
                    ng-model="model.image_2">
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-3 control-label">URL картинка 3</label>
        <div class="col-sm-9">
            <input
                    type="text"
                    class="form-control"
                    ng-model="model.image_3">
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-3 control-label">URL картинка 4</label>
        <div class="col-sm-9">
            <input
                    type="text"
                    class="form-control"
                    ng-model="model.image_4">
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-3 control-label">Youtube ID</label>
        <div class="col-sm-9">
            <input
                    type="text"
                    class="form-control"
                    ng-model="model.video">
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-3 control-label">Удален</label>
        <div class="col-sm-9">
            <input
                    type="checkbox"
                    ng-model="model.is_deleted"
                    ng-true-value="1"
                    ng-false-value="0">
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-9">
            <button type="submit" class="btn btn-default">Сохранить</button>
        </div>
    </div>
</form>
