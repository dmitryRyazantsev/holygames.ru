<form class="form-horizontal" role="form" ng-submit="update()">
    <div class="form-group">
        <label class="col-sm-2 control-label">Игра</label>
        <div class="col-sm-10">
            <input
                    type="text"
                    class="form-control"
                    ng-model="game.name"
                    disabled>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Кол-во участников</label>
        <div class="col-sm-10">
            <input
                    type="text"
                    class="form-control"
                    ng-model="model.amount_users">
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-2 control-label">Цена для участника</label>
        <div class="col-sm-10">
            <input
                    type="text"
                    class="form-control"
                    ng-model="model.price">
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-2 control-label">Опубликовано</label>
        <div class="col-sm-10">
            <input
                    type="checkbox"
                    ng-model="model.is_publish"
                    ng-true-value="'1'"
                    ng-false-value="'0'">
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-default">Сохранить</button>
        </div>
    </div>
</form>
