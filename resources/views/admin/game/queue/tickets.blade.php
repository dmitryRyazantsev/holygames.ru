@extends('AdminLTE.layout')
@section('page-header')
    Очередь
    {{ $model->game->name }}
    ({{ $model->amount_users }}) - Билеты
@overwrite
@section('page-description')
    Все
@overwrite
@section('content')
    <div class='row'>
        <div class='col-md-8'>
            <!-- Box -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Билеты</h3>

                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                        <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <table id="genreTable"
                           class="table table-striped table-bordered"
                           cellspacing="0"
                           width="100%">
                        <thead>
                            <tr>
                                <th>id</th>
                                <th>Email</th>
                                <th>Дата</th>
                                <th>Победитель</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($model->tickets as $key => $ticket)
                                <tr>
                                    <td>{{ $ticket->id }}</td>
                                    <td>{{ $ticket->email }}</td>
                                    <td>{{ (new DateTime($ticket->created_at))->format('d-m-Y') }}</td>
                                    <td>
                                        @if($model->status == \App\Domain\Models\Queue::STATUS_IN_PROGRESS)
                                            <a href="{{ route('admin.game.queue.ticket', [$model->game_id, $model->id, $ticket->id]) }}">
                                                <button class="btn btn-success btn-flat"
                                                        {{ (count($model->tickets) == $model->amount_users) ? '' : 'disabled' }}
                                                        >
                                                    Выбрать
                                                </button>
                                            </a>
                                        @else
                                            <span class="label {{ $ticket->status == \App\Domain\Models\Ticket::STATUS_WINNER ? 'label-success' : 'label-default' }}"
                                                  style="padding: 5px 10px;">
                                                Победитель
                                            </span>
                                        @endif
                                    </td>

                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
@overwrite
@section('scripts')
<script>
    var tickets = {!! json_encode($model->tickets) !!}
</script>
@overwrite
