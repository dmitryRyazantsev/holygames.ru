@extends('AdminLTE.layout')
@section('page-header')
    Очереди
@overwrite
@section('page-description')
    Изменить очередь
@overwrite
@section('content')
    <div class='row'>
        <div class='col-md-8'>
            <!-- Box -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Редактирование очереди {{ $model->name }}</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                        <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="box-body" ng-controller="QueueEditCtrl">
                        @include('admin.game.queue.form')
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
@overwrite
@section('scripts')
<script>
    var model = {!! json_encode($model) !!};
    var game = {!! json_encode($game) !!};
</script>
@overwrite
