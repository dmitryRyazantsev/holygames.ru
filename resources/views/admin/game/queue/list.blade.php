@extends('AdminLTE.layout')
@section('page-header')
    Очереди {{ $model->name }}
@overwrite
@section('page-description')
    Все
@overwrite
@section('content')
    <div class='row'>
        <div class='col-md-12'>
            <!-- Box -->
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Очереди {{ $model->name }}</h3>
                    <a href="{{ route('admin.game.queue.create', [ $model->id ]) }}"
                       class="btn btn-info btn-sm"
                       style="margin-left: 1em;">
                        Создать
                    </a>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                        <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="box-body"
                     ng-controller="QueueListCtrl">
                    <table id="gameQueueTable"
                           class="table table-striped table-bordered"
                           cellspacing="0"
                           width="100%">
                        <thead>
                            <tr>
                                <th>id</th>
                                <th>Кол-во людей</th>
                                <th>Цена</th>
                                <th>Продано мест</th>
                                <th>Опубликовано</th>
                                <th>Статус</th>
                                <th>Билеты</th>
                                <th>Изменить</th>
                                <th>Удалить</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($model->allQueue as $key => $queue)
                            <tr>
                                <td>{{ $queue->id }}</td>
                                <td>{{ $queue->amount_users }}</td>
                                <td>{{ $queue->price }} руб</td>
                                <td>{{ count($queue->tickets) }}</td>
                                <td style="text-align: center;">
                                        <span class="label {{ $queue->is_publish ? 'label-success' : 'label-default' }}"
                                                style="padding: 5px 10px; ">
                                            {{ $queue->is_publish ? 'Да' : 'Нет' }}
                                        </span>
                                </td>
                                <td style="text-align: center;">
                                        <span class="label {{ $queue->status == 2 ? 'label-success' : 'label-default' }}"
                                              style="padding: 5px 10px; ">
                                            {{ $queue->status == 2 ? 'Завершено' : 'В процессе' }}
                                        </span>
                                </td>
                                <td>
                                    <a href="{{ route('admin.game.queue.ticket.index', [ $queue->game_id, $queue->id ]) }}">
                                        <button
                                                class="btn btn-{{ (count($queue->tickets) == $queue->amount_users) &&  $queue->is_publish ? 'warning' : 'primary'  }} btn-flat"
                                                >
                                            Билеты
                                        </button>
                                    </a>
                                </td>
                                <td>
                                    <a href="{{ route('admin.game.queue.edit', [ $queue->game_id, $queue->id ]) }}">
                                        <button class="btn btn-primary btn-flat">
                                            Изменить
                                        </button>
                                    </a>
                                </td>
                                <td style="width: 150px">
                                    <a class="btn btn-danger"
                                       ng-click="confirm({{ $key }})">
                                       Удалить
                                    </a>
                                    <a class="btn btn-danger"
                                       ng-show="queues[{{ $key }}].confirmed"
                                       ng-click="delete({{ $queue->game_id }},{{ $queue->id }})">
                                       Ок
                                    </a>

                                </td>

                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
@overwrite
@section('scripts')
<script>
    var queues = {!! json_encode($model->allQueue)  !!}
</script>
@overwrite
