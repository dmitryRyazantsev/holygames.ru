@extends('AdminLTE.layout')
@section('page-header')
    Игры
@overwrite
@section('page-description')
    Новая Игра
@overwrite
@section('content')
    <div class='row'>
        <div class='col-md-8'>
            <!-- Box -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Новая Игра</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                        <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="box-body" ng-controller="GameCreateCtrl">
                        @include('admin.game.form')
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
@overwrite
@section('scripts')
<script>
    var genres = {!! json_encode($genres) !!}
    var categories = {!! json_encode($categories) !!}
</script>
@overwrite
