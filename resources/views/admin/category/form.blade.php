<form class="form-horizontal" role="form" ng-submit="update()">
    <div class="form-group">
        <label class="col-sm-2 control-label">Название</label>
        <div class="col-sm-10">
            <input
                    type="text"
                    class="form-control"
                    placeholder="Название"
                    ng-model="model.name">
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-2 control-label">Удален</label>
        <div class="col-sm-10">
            <input
                    type="checkbox"
                    ng-model="model.is_deleted"
                    ng-true-value="1"
                    ng-false-value="0">
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-default">Сохранить</button>
        </div>
    </div>
</form>
