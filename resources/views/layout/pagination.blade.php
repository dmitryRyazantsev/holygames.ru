<?php
    $perPage = isset($perPage) ? $perPage : 10;
    $linkParams = '';

    if(isset($params)){
        foreach($params as $param => $value){
            if($param == 'page'){
                continue;
            }
            $linkParams .= '&'.$param.'='.$value;
        }
    }
?>

@for($i = 1; $i <= ceil($total / $perPage); $i++)
    @if(($i == $page))
        <a
                style="padding: 1px 3px;background:#494949;color:#7F7F7F;"
                href="{{ route($route.'.index') }}?page=1{{ $linkParams }}">
            <strong style="color: #fff">
                {{ $i }}
            </strong>
        </a>
    @else
        <a
                style="padding: 1px 3px;background:#494949;color:#7F7F7F;"
                href="{{ route($route.'.index') }}?page={{ $i }}{{ $linkParams }}">
            <strong style="color: #fff">
                {{ $i }}
            </strong>
        </a>
    @endif
@endfor
