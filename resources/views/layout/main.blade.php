<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="/css/boilerplate.css">
    <link rel="stylesheet" href="/css/page.css">
    <meta charset="utf-8">
    <meta name="viewport" content="initial-scale = 1.0,maximum-scale = 1.0">
    <title>@section('page-title') Holygames.ru - покупка игр по самым низким ценам @show</title>
    <meta name="description" content="@section('page-description') @show">
    <meta name="keywords" content="">
    <meta name="robots" content="">
</head>
<body>

<div id="primaryContainer" class="primaryContainer clearfix">
    <div id="box" class="clearfix">
        <img src="/img/logo.png">
        <div class="menu">
            @include('layout.menu')
        </div>
    </div>
    <div id="box1" class="clearfix">
        <div id="box2" class="clearfix">
            @include('layout.top-menu')
        </div>
        <div id="box3" class="clearfix">
            @include('layout.search')
        </div>
        <div id="box4" class="clearfix">
            @section('content')
            @show
        </div>
    </div>
    <div id="box5" class="clearfix">
        <div id="box6" class="clearfix">
            <div class="menu">
                @include('layout.footer')
            </div>
        </div>
    </div>
</div>
</body>
</html>
