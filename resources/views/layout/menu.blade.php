<p>По категориям:</p>
<ul>
    @foreach($categories as $category)
    <li>
        <a href="{{ route('game.index') }}?category={{ $category->id }}">{{ $category->name }}</a>
    </li>
    @endforeach
</ul>
<p>По жанрам:</p>
<ul>
    @foreach($genres as $genre)
        <li>
            <a href="{{ route('game.index') }}?genre={{ $genre->id }}">{{ $genre->name }}</a>
        </li>
    @endforeach
</ul>
