<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
    <meta charset="UTF-8">
    <title>AdminLTE 2 | Dashboard</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.2 -->
    <link href="{{ asset("/bower_components/AdminLTE/bootstrap/css/bootstrap.min.css") }}" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="{{ asset("/css/AdminLTE/font-awesome.min.css") }}" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="{{ asset("/css/AdminLTE/ionicons.min.css") }}" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="{{ asset("/bower_components/AdminLTE/dist/css/AdminLTE.min.css")}}" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect.
    -->
    <link href="{{ asset("/bower_components/AdminLTE/dist/css/skins/skin-blue.min.css")}}" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body class="skin-blue" ng-app="AdminModule">
<div class="wrapper">

    <!-- Main Header -->
    @include('AdminLTE.header')
    <!-- Left side column. contains the logo and sidebar -->
    @include('AdminLTE.left-side')
    <!-- Content Wrapper. Contains page content -->
    @include('AdminLTE.content')
    <!-- Main Footer -->
    @include('AdminLTE.footer')

</div><!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 2.1.4 -->
<script src="{{ asset ("/bower_components/AdminLTE/plugins/jQuery/jQuery-2.1.4.min.js") }}"></script>
<!-- Bootstrap 3.3.2 JS -->
<script src="{{ asset ("/bower_components/AdminLTE/bootstrap/js/bootstrap.min.js") }}" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="{{ asset ("/bower_components/AdminLTE/dist/js/app.min.js") }}" type="text/javascript"></script>

<!-- DataTables -->
<link href="{{ asset("/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.css") }}" rel="stylesheet" type="text/css" />
<script src="{{ asset ("/bower_components/AdminLTE/plugins/datatables/jquery.dataTables.min.js") }}" type="text/javascript"></script>
<script src="{{ asset ("/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.min.js") }}" type="text/javascript"></script>

<script src="{{ asset ("/bower_components/angular/angular.min.js") }}"></script>
<script src="{{ asset ("/bower_components/lodash/lodash.min.js") }}"></script>
<script src="{{ asset ("/bower_components/restangular/dist/restangular.min.js") }}"></script>
<script src="{{ asset ("/bower_components/angular-datatables/dist/angular-datatables.min.js") }}"></script>

{{--JS APP--}}
<script src="{{ asset ("/app/app.js") }}"></script>
<script src="{{ asset ("/app/HolygameServices.js") }}"></script>

{{--Services--}}
<script src="{{ asset ("/app/Services/GenreService.js") }}"></script>
<script src="{{ asset ("/app/Services/CategoryService.js") }}"></script>
<script src="{{ asset ("/app/Services/GameService.js") }}"></script>
<script src="{{ asset ("/app/Services/QueueService.js") }}"></script>

{{--Modules--}}
<script src="{{ asset ("/app/Modules/GenreModule.js") }}"></script>
<script src="{{ asset ("/app/Modules/CategoryModule.js") }}"></script>
<script src="{{ asset ("/app/Modules/GameModule.js") }}"></script>
<script src="{{ asset ("/app/Modules/QueueModule.js") }}"></script>

@section('scripts')

@show
<!-- Optionally, you can add Slimscroll and FastClick plugins.
      Both of these plugins are recommended to enhance the
      user experience -->
</body>
</html>
