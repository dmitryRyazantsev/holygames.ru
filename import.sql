INSERT INTO holygames.games (
  `name`,
  price,
  description,
  language,
    release_date,
    vendor,
    shop,
    image_preview,
    image_view,
    image_1,
    image_2,
    image_3,
    image_4,
    video,
    `is_deleted`
)
  SELECT
    gamename,
    fullcost,
    `desc`,
    par4,
    par1,
    par2,
    par5,
    url,
    headurl,
    img1,
    img2,
    img3,
    img4,
    video,
    0 as is_deleted
  FROM holygames_old.gb_games as old
  WHERE old.fullcost != 0;
